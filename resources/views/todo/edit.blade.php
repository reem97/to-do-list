@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="container">
                    <div class="jumbotron">
                        <h1 class="display-4">To Do List</h1>
                        <form method="POST" action="{{ route('todo.update', ['id' => $task->id]) }}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="title" class="form-control" value="{{ $task->title }}">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Update </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
