@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="container">
                    <div class="jumbotron">
                        <h1 class="display-4">To Do List</h1>
                        <form method="POST" action="{{ route('todo.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="title" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Create </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @if ($tasks->count() > 0)
                <div class="col">
                    @php
                        $i = 0;
                    @endphp
                    <div class="container">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Done</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tasks as $item)
                                    <tr>
                                        <th scope="row">{{ ++$i }}</th>
                                        <td>
                                            <form method="POST"
                                                action="{{ route('todo.completed', ['id' => $item->id]) }}"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <input type="checkbox" onclick="this.form.submit()" name="completed"
                                                    @if ($item->completed == '1') checked @endif>
                                            </form>
                                        </td>
                                        @if ($item->completed == false)
                                            <td scope="col">{{ $item->title }}</td>
                                        @else
                                            <td scope="col">
                                                <del>{{ $item->title }}</del>
                                            </td>
                                        @endif

                                        <td scope="col">
                                            @if ($item->user_id == Auth::id())
                                                <a href="{{ route('todo.edit', ['id' => $item->id]) }}">edit</a>
                                                &nbsp;&nbsp;
                                                <a href="{{ route('todo.destroy', ['id' => $item->id]) }}">delete</a>
                                            @endif

                                        </td>

                                    </tr>
                                @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            @else
                <div class="col">
                    <div class="alert alert-danger" role="alert">
                        Nothing to do
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
