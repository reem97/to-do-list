<?php

namespace App\Http\Controllers;

use App\Models\ToDo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ToDoController extends Controller
{

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Undocumented function
     *
     * @return void
     */
    public function index()
    {
        $tasks = ToDo::where('user_id', Auth::id())->get();
        return view('todo.index')->with("tasks", $tasks);
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);
        $id = Auth::id();
        $task = ToDo::create([
            'user_id' => $id,
            'title' => $request->title,
        ]);

        return redirect()->back();
    }


    /**
     * Undocumented function
     *
     * @param integer $id
     * @return boolean
     */
    public function isCompleted(int $id)
    {
        $task = ToDo::findOrFail($id);
        $task->completed = ($task->completed == false) ? true : false;
        $task->save();
        return redirect()->back();
    }



    /**
     * Undocumented function
     *
     * @param integer $id
     * @return void
     */
    public function edit(int $id)
    {
        $task = ToDo::findOrFail($id);
        return view('todo.edit')->with("task", $task);
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @param integer $id
     * @return void
     */
    public function update(Request $request, int $id)
    {
        $task = ToDo::findOrFail($id);
        $this->validate($request, [
            'title' => 'required'
        ]);

        $task->title = $request->title;
        $task->save();
        return redirect()->route('todo');
    }


    /**
     * Undocumented function
     *
     * @param integer $id
     * @return void
     */
    public function destroy(int $id)
    {
        $task = ToDo::findOrFail($id);
        $task->delete();
        return redirect()->route('todo');
    }
}
