<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


//Routes for ToDo list

Route::get('/todo', 'ToDoController@index')->name('todo');
Route::get('/todo/create', 'ToDoController@create')->name('todo.create');
Route::post('/todo/store', 'ToDoController@store')->name('todo.store');
Route::post('/todo/completed/{id}', 'ToDoController@isCompleted')->name('todo.completed');
Route::get('/todo/edit/{id}', 'ToDoController@edit')->name('todo.edit');
Route::post('/todo/update/{id}', 'ToDoController@update')->name('todo.update');
Route::get('/todo/destroy/{id}', 'ToDoController@destroy')->name('todo.destroy');
